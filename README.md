# 주민 대형폐기물 처리 AI 서비스
  2022 동국대학교 창의문제해결 프로젝트 
  팀 아예스 (김수민, 김시연, 나은수, 백채연)


## 🛠️ 개발환경
- Node.js
- MongoDB
- AWS

## 🧩 DB ER Diagram
![DGU CMH APP ERD](https://user-images.githubusercontent.com/77263479/189514458-edff3173-a997-41c2-96ae-0ae0b5c56d3b.jpeg)

## ✍️ 주요기능
- 카메라 촬영 후 대형폐기물 종류 분석
- 분석 후 폐기물 정보 확인
- 처리 선택 후 폐기물 처리 업체와 컨택
